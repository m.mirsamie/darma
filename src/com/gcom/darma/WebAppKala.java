package com.gcom.darma;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class WebAppKala {
	Context mContext;
	KalaClass data;
	String ajaxout="";
	String javaScriptFn = "";
    String javaScriptFailFn = "";
    Boolean ajaxReady = true;
    int kala_miniGroup_id=-1;
	public WebAppKala(Context c,int inKala_miniGroup_id) {
		mContext = c;
		kala_miniGroup_id = inKala_miniGroup_id;
		data = new KalaClass(c,-1,-1, "","","",0);
		data.open();
	}
	
	@JavascriptInterface
	public String getLink()
	{
		String out = MainActivity.server_link;
		return(out);
	}
	
	@JavascriptInterface
	public void toast(String inp)
	{
		Toast.makeText(mContext, inp, Toast.LENGTH_SHORT).show();
	}

	@JavascriptInterface
	public void toastLong(String inp)
	{
		Toast.makeText(mContext, inp, Toast.LENGTH_LONG).show();
	}
	
	@JavascriptInterface
	public String loadList()
	{
		String out = "[]";
		List<KalaClass> dataList = data.getList(kala_miniGroup_id);
		String[] outData = new String[dataList.size()];
		
		for(int i = 0;i < dataList.size();i++)
			outData[i] = String.valueOf(dataList.get(i).id)+"|"+dataList.get(i).name+"|"+dataList.get(i).ghimat;
		try {
			JSONArray ja = new JSONArray(Arrays.asList(outData));
			out = ja.toString();
			
		} catch (Exception e) {
		}
		return out;
	}
	
	@JavascriptInterface
	public String add(int id,int inKala_miniGroup_id,String name,String toz,String pic,int ghimat)
	{
		data.id = id;
		data.kala_miniGroup_id = inKala_miniGroup_id;
		data.name = name;
		data.toz=toz;
		data.pic = pic;
		data.ghimat=ghimat;
		return data.add();
	}
	@JavascriptInterface
	public Boolean truncate()
	{
		data.trunTable();
		return true;
	}
	@JavascriptInterface
	public void addToSabad(int kala_id,int tedad)
	{
		
	}
	//-------------------------AJAX--------------------------------------------------
    @JavascriptInterface
    public String getAjax(String url,String jscript)
    {
    	String out = "false";
    	final ajax aj = new ajax(mContext);
    	String[] tmp = jscript.split(",");
    	javaScriptFn = tmp[0];
    	if(tmp.length>1)
    		javaScriptFailFn = tmp[1];
    	aj.execute(url);
    	out = "true";
    	return out;
    }
    @JavascriptInterface
    public String ajaxResponse()
    {
    	if(ajaxReady)
    		return ajaxout;
    	else
    		return "NaN";
    }
    @JavascriptInterface
    public String getSabad()
    {
    	return(MainActivity.sabad);
    }
    @JavascriptInterface
    public void setSabad(String inp)
    {
    	MainActivity.sabad=inp;
    }
    @JavascriptInterface
	public void loadSabad()
	{
		Intent intent = new Intent();
		intent.setClass(mContext,SabadActivity.class);
		mContext.startActivity(intent);
	}
	private class ajax extends AsyncTask<String, Integer, String> {

	    private Context context;
	    public ajax(Context context) {
	        this.context = context;
	    }

	    @SuppressLint({ "SdCardPath", "Wakelock" })
		@Override
	    protected String doInBackground(String... sUrl) {
	    	String out = "";
	        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
	             getClass().getName());
	        wl.acquire();
	        try {
	        	ajaxout = "";
	            InputStream input = null;
	            HttpURLConnection connection = null;
	            try {
	                URL url = new URL(sUrl[0]);
	                connection = (HttpURLConnection) url.openConnection();
	                connection.connect();
	                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
	                {
	                     out = "Server returned HTTP " + connection.getResponseCode() 
	                         + " " + connection.getResponseMessage();
	                     return out;
	                }
	                input = connection.getInputStream();
	                BufferedReader in = new BufferedReader(new InputStreamReader(input));
	                String line = null;
	                while ((line = in.readLine()) != null) {
	                	ajaxout += line;
	                }
	                in.close();
	            } catch (Exception e) {
	                return e.toString();
	            } finally {
	                try {
	                    if (input != null)
	                        input.close();
	                } 
	                catch (IOException ignored) { }

	                if (connection != null)
	                    connection.disconnect();
	            }
	        } finally {
	            wl.release();
	        }
	        return null;
	    }
	    @Override
	    protected void onPostExecute(String result) {
			KalaActivity ma = (KalaActivity) mContext;
	        if (result == null)
	        {
				try {
					ajaxReady = true;
					if(javaScriptFn != "")
						ma.mywebview.loadUrl("javascript:"+javaScriptFn+"('"+ajaxout+"');");
				} catch (Exception e) {
					if(javaScriptFailFn != "")
						ma.mywebview.loadUrl("javascript:"+javaScriptFailFn+"('"+result+"');");
				}
	        }
	        else
	        	if(javaScriptFailFn != "")
	        		ma.mywebview.loadUrl("javascript:"+javaScriptFailFn+"('"+result+"');");
	    }
	}
}
