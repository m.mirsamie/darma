package com.gcom.darma;

import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class KalaMiniGroupActivity extends Activity {

	WebView mywebview;
	public static String newVersion = "";
	public static int currentVersion = 0;
	
	@SuppressLint({ "SetJavaScriptEnabled", "JavascriptInterface" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kala_mini_group);
		mywebview = (WebView) findViewById(R.id.mini_group_webview);
		WebSettings webSettings = mywebview.getSettings();
		Intent inten = getIntent();
		int kala_group_id = inten.getIntExtra("kala_group_id", -1);
		mywebview.addJavascriptInterface(new WebAppKalaMiniGroup(this,kala_group_id), "kala_mini_group");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/html/activity_kala_minigroup.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}
}
