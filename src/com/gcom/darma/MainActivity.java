package com.gcom.darma;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

public class MainActivity extends Activity {
	private Integer SPLASH_DISPLAY_TIME = 3000;
	static String sabad="[]";
	public static String server_link = "http://darma.ir/sale/";//"http://192.168.1.80/darma/";  

	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
            		Intent intent = new Intent(MainActivity.this,KalaAbarGroupActivity.class);
    	        	startActivity(intent);
            }
        }, SPLASH_DISPLAY_TIME);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
            	//Intent intent = new Intent(MainActivity.this,KalaAbarGroupActivity.class);
    	        //startActivity(intent);
            }
        }, SPLASH_DISPLAY_TIME);
    }
}
