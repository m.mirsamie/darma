package com.gcom.darma;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class KalaAbarGroupClass {
	public int id = -1;
	public String name = "";
	public int en = 1;
	public int ord = 1;
	private Context mContext;
	private SQLiteDatabase database;
	private msqlite dbHelper;
	private String[] allColumns = {"id","name","en","ord"};
	public KalaAbarGroupClass(Context inContext,int inId,String inName,int inEn,int inOrd) {
		mContext = inContext;
		id = inId;
		name = inName;
		en = inEn;
		ord = inOrd;
		dbHelper = new msqlite(inContext);
	}
	public void open() throws SQLException {
		try
		{
			database = dbHelper.getWritableDatabase();
		}catch (Exception e) {
		}
	}
	public void close() {
		dbHelper.close();
	}
	public KalaAbarGroupClass cursorToKalaAbarGroupClass(Cursor cursor)
	{
		KalaAbarGroupClass out = new KalaAbarGroupClass(mContext,cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getInt(3));
		return out;
	}
	public List<KalaAbarGroupClass> getList()
	{
		List<KalaAbarGroupClass> out = new ArrayList<KalaAbarGroupClass>();
		try {
			Cursor cursor = database.query(msqlite.KALA_ABARGROUP_TABLE_NAME,allColumns, null, null, null, null, "ord");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				KalaAbarGroupClass newpr = cursorToKalaAbarGroupClass(cursor);
				out.add(newpr);
				cursor.moveToNext();
			}
			cursor.close();
		} catch (Exception e) {
		}
		return out;
	}
	public String add()
	{
		  ContentValues values = new ContentValues();
		  values.put("id", this.id);
		  values.put("name", this.name);
		  values.put("en", this.en);
		  values.put("ord", this.ord);
		  long i = database.insert(msqlite.KALA_ABARGROUP_TABLE_NAME, null,values);
		  return String.valueOf(i);
	}
}
