package com.gcom.darma;

import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class SabadActivity extends Activity {
	WebView mywebview;
	public static String newVersion = "";
	public static int currentVersion = 0;
	
	@SuppressLint({ "SetJavaScriptEnabled", "JavascriptInterface" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kala);
		mywebview = (WebView) findViewById(R.id.kala_webview);
		WebSettings webSettings = mywebview.getSettings();
		Intent inten = getIntent();
		String data = inten.getStringExtra("data");
		mywebview.addJavascriptInterface(new WebAppSabad(this,data), "sabad");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/html/activity_sabad.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}

}
