package com.gcom.darma;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class KalaClass {
	public int id = -1;
	public int kala_miniGroup_id = -1;
	public String name = "";
	public String toz = "";
	public String pic = "";
	public int ghimat = 0;
	private Context mContext;
	private SQLiteDatabase database;
	private msqlite dbHelper;
	private String[] allColumns = {"id","kala_miniGroup_id","name","toz","pic","ghimat"};
	public KalaClass(Context inContext,int inId,int inKala_miniGroup_id,String inName,String inToz,String inPic,int inGhimat) {
		mContext = inContext;
		id = inId;
		kala_miniGroup_id = inKala_miniGroup_id;
		name = inName;
		toz = inToz;
		pic = inPic;
		ghimat = inGhimat;
		dbHelper = new msqlite(inContext);
	}
	public void open() throws SQLException {
		try
		{
			database = dbHelper.getWritableDatabase();
		}catch (Exception e) {
		}
	}
	public void close() {
		dbHelper.close();
	}
	public KalaClass cursorToKalaClass(Cursor cursor)
	{
		KalaClass out = new KalaClass(mContext,cursor.getInt(0),cursor.getInt(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getInt(5));
		return out;
	}
	public List<KalaClass> getList()
	{
		List<KalaClass> out = new ArrayList<KalaClass>();
		try {
			Cursor cursor = database.query(msqlite.KALA_TABLE_NAME,allColumns, null, null, null, null, "name");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				KalaClass newpr = cursorToKalaClass(cursor);
				out.add(newpr);
				cursor.moveToNext();
			}
			cursor.close();
		} catch (Exception e) {
		}
		return out;
	}
	public List<KalaClass> getList(int inKala_miniGroup_id)
	{
		List<KalaClass> out = new ArrayList<KalaClass>();
		try {
			Cursor cursor = database.query(msqlite.KALA_TABLE_NAME,allColumns, " kala_miniGroup_id = "+inKala_miniGroup_id, null, null, null, "name");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				KalaClass newpr = cursorToKalaClass(cursor);
				out.add(newpr);
				cursor.moveToNext();
			}
			cursor.close();
		} catch (Exception e) {
		}
		return out;
	}
	public String add()
	{
		
		database.execSQL("insert into "+msqlite.KALA_TABLE_NAME+" (id,kala_miniGroup_id,name,toz,pic,ghimat) values ("+this.id+","+this.kala_miniGroup_id+",'"+this.name+"','"+this.toz+"','"+this.pic+"',"+this.ghimat+")");
		return "true";
	}
	public void trunTable()
	{
		database.execSQL("delete from "+msqlite.KALA_TABLE_NAME+" where 1=1 ");
	}
}
