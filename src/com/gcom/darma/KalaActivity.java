package com.gcom.darma;

import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class KalaActivity extends Activity {

	WebView mywebview;
	public static String newVersion = "";
	public static int currentVersion = 0;
	
	@SuppressLint({ "SetJavaScriptEnabled", "JavascriptInterface" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kala);
		mywebview = (WebView) findViewById(R.id.kala_webview);
		WebSettings webSettings = mywebview.getSettings();
		Intent inten = getIntent();
		int kala_miniGroup_id = inten.getIntExtra("kala_miniGroup_id", -1);
		mywebview.addJavascriptInterface(new WebAppKala(this,kala_miniGroup_id), "kala");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/html/activity_kala.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();
		mywebview.loadUrl("javascript:refreshJam();");
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mywebview.loadUrl("javascript:refreshJam();");
	}
}
