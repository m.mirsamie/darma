package com.gcom.darma;

import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class KalaGroupActivity extends Activity {

	WebView mywebview;
	public static String newVersion = "";
	public static int currentVersion = 0;
	
	@SuppressLint({ "SetJavaScriptEnabled", "JavascriptInterface" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kala_group);
		mywebview = (WebView) findViewById(R.id.group_webview);
		WebSettings webSettings = mywebview.getSettings();
		Intent inten = getIntent();
		int kala_abarGroup_id = inten.getIntExtra("kala_abarGroup_id", -1);
		mywebview.addJavascriptInterface(new WebAppKalaGroup(this,kala_abarGroup_id), "kala_group");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/html/activity_kala_group.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}
}
