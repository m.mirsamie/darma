package com.gcom.darma;

import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.view.Menu;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class KalaAbarGroupActivity extends Activity {
	WebView mywebview;
	public static String newVersion = "";
	public static int currentVersion = 0;
	
	@SuppressLint({ "SetJavaScriptEnabled", "JavascriptInterface" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kala_abar_group);
		mywebview = (WebView) findViewById(R.id.abar_webview);
		WebSettings webSettings = mywebview.getSettings();
		mywebview.addJavascriptInterface(new WebAppKalaAbarGroup(this), "Android");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/html/new_darma.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.kala_abar_group, menu);
		return true;
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		System.exit(currentVersion);
	}
}
