package com.gcom.darma;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class KalaGroupClass {
	public int id = -1;
	public int kala_abarGroup_id = -1;
	public String name = "";
	public String toz = "";
	public int en = 1;
	public int ord = 0;
	public String pic = "";
	private Context mContext;
	private SQLiteDatabase database;
	private msqlite dbHelper;
	private String[] allColumns = {"id","kala_abarGroup_id","name","toz","en","ord","pic"};
	public KalaGroupClass(Context inContext,int inId,int inKala_abarGroup_id,String inName,String inToz,int inEn,int inOrd,String inPic) {
		mContext = inContext;
		id = inId;
		kala_abarGroup_id = inKala_abarGroup_id;
		name = inName;
		toz = inToz;
		en = inEn;
		ord = inOrd;
		pic = inPic;
		dbHelper = new msqlite(inContext);
	}
	public void open() throws SQLException {
		try
		{
			database = dbHelper.getWritableDatabase();
		}catch (Exception e) {
		}
	}
	public void close() {
		dbHelper.close();
	}
	public KalaGroupClass cursorToKalaGroupClass(Cursor cursor)
	{
		KalaGroupClass out = new KalaGroupClass(mContext,cursor.getInt(0),cursor.getInt(1),cursor.getString(2),cursor.getString(3),cursor.getInt(4),cursor.getInt(5),cursor.getString(6));
		return out;
	}
	public List<KalaGroupClass> getList()
	{
		List<KalaGroupClass> out = new ArrayList<KalaGroupClass>();
		try {
			Cursor cursor = database.query(msqlite.KALA_GROUP_TABLE_NAME,allColumns, null, null, null, null, "ord");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				KalaGroupClass newpr = cursorToKalaGroupClass(cursor);
				out.add(newpr);
				cursor.moveToNext();
			}
			cursor.close();
		} catch (Exception e) {
		}
		return out;
	}
	public List<KalaGroupClass> getList(int kala_abarGroup_id)
	{
		List<KalaGroupClass> out = new ArrayList<KalaGroupClass>();
		try {
			Cursor cursor = database.query(msqlite.KALA_GROUP_TABLE_NAME,allColumns, " kala_abarGroup_id = "+kala_abarGroup_id, null, null, null, "ord");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				KalaGroupClass newpr = cursorToKalaGroupClass(cursor);
				out.add(newpr);
				cursor.moveToNext();
			}
			cursor.close();
		} catch (Exception e) {
		}
		return out;
	}
	public String add()
	{
		  database.execSQL("insert into "+msqlite.KALA_GROUP_TABLE_NAME+" (id,kala_abarGroup_id,name,toz,en,ord,pic) values ( "+this.id+","+this.kala_abarGroup_id+",'"+this.name+"','"+this.toz+"',"+this.en+","+this.ord+",'"+this.pic+"' )");
		  return "true";
	}
}
