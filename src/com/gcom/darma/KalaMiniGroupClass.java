package com.gcom.darma;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class KalaMiniGroupClass {
	public int id = -1;
	public int kala_group_id = -1;
	public String name = "";
	public String pic = "";
	private Context mContext;
	private SQLiteDatabase database;
	private msqlite dbHelper;
	private String[] allColumns = {"id","kala_group_id","name","pic"};
	public KalaMiniGroupClass(Context inContext,int inId,int inKala_group_id,String inName,String inPic) {
		mContext = inContext;
		id = inId;
		kala_group_id = inKala_group_id;
		name = inName;
		pic = inPic;
		dbHelper = new msqlite(inContext);
	}
	public void open() throws SQLException {
		try
		{
			database = dbHelper.getWritableDatabase();
		}catch (Exception e) {
		}
	}
	public void close() {
		dbHelper.close();
	}
	public KalaMiniGroupClass cursorToKalaMiniGroupClass(Cursor cursor)
	{
		KalaMiniGroupClass out = new KalaMiniGroupClass(mContext,cursor.getInt(0),cursor.getInt(1),cursor.getString(2),cursor.getString(3));
		return out;
	}
	public List<KalaMiniGroupClass> getList()
	{
		List<KalaMiniGroupClass> out = new ArrayList<KalaMiniGroupClass>();
		try {
			Cursor cursor = database.query(msqlite.KALA_MINIGROUP_TABLE_NAME,allColumns, null, null, null, null, "name");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				KalaMiniGroupClass newpr = cursorToKalaMiniGroupClass(cursor);
				out.add(newpr);
				cursor.moveToNext();
			}
			cursor.close();
		} catch (Exception e) {
		}
		return out;
	}
	public List<KalaMiniGroupClass> getList(int inKala_group)
	{
		List<KalaMiniGroupClass> out = new ArrayList<KalaMiniGroupClass>();
		try {
			Cursor cursor = database.query(msqlite.KALA_MINIGROUP_TABLE_NAME,allColumns, "kala_group_id = "+inKala_group, null, null, null, "name");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				KalaMiniGroupClass newpr = cursorToKalaMiniGroupClass(cursor);
				out.add(newpr);
				cursor.moveToNext();
			}
			cursor.close();
		} catch (Exception e) {
		}
		return out;
	}
	public String add()
	{
		database.execSQL("insert into "+msqlite.KALA_MINIGROUP_TABLE_NAME+" (id,kala_group_id,name,pic) values ("+this.id+","+this.kala_group_id+",'"+this.name+"','"+this.pic+"')");
		return "true";
	}
}
